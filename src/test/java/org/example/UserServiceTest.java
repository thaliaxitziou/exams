package org.example;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Autowired
    UserService userService = new UserService();

    @Mock
    UserDb userDb = Mockito.mock(UserDb.class);

    @Test
    public void shouldReturnTrueIfPasswordIs8rMoreCharacters() throws Exception {

        Mockito.when(userDb.changePassword("Thalia","123456789")).thenReturn(true);
        Boolean expected = userService.changePassword("Thalia","123456789");
        assertEquals(true,expected);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionForWeakPassword() throws Exception{
        userService.changePassword("Thalia","1234567");
        fail("Exception expected");
    }
}